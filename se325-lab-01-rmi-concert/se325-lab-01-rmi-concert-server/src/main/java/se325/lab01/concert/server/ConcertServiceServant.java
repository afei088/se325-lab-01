package se325.lab01.concert.server;

import se325.lab01.concert.common.Concert;
import se325.lab01.concert.common.ConcertService;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ConcertServiceServant extends UnicastRemoteObject implements ConcertService {
    // List of Concerts.
    private Map<Long, Concert> concerts;

    // Unique id of the next concert to create.
    private long nextId;

    ConcertServiceServant() throws RemoteException {
        concerts = new HashMap<>();
        nextId = 1;
    }

    @Override
    public Concert createConcert(Concert concert) throws RemoteException {
        // Store the new Concert.
        Long uid = nextId++;
        Concert newConcert = new Concert(uid, concert.getTitle(), concert.getDate());
        concerts.put(uid, newConcert);
        return newConcert;
    }

    @Override
    public Concert getConcert(Long id) throws RemoteException {
        Concert concert = concerts.get(id);
        return concert;
    }

    @Override
    public boolean updateConcert(Concert concert) throws RemoteException {
        if (!concerts.containsKey(concert.getId())) {
            return false;
        } else {
            concerts.put(concert.getId(), concert);
            return true;
        }
    }

    @Override
    public boolean deleteConcert(Long id) throws RemoteException {
        Concert concert = concerts.get(id);
        if (concert == null) {
            return false;
        } else {
            concerts.remove(id);
            return true;
        }
    }

    @Override
    public List<Concert> getAllConcerts() throws RemoteException {
        List<Concert> concertList = new ArrayList<>(concerts.values());
        return concertList;
    }

    @Override
    public void clear() throws RemoteException {
        concerts.clear();
    }
}
