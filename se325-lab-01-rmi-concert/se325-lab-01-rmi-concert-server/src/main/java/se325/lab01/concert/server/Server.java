package se325.lab01.concert.server;

import se325.lab01.concert.common.ConcertService;

import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

public class Server {

    public static void main(String[] args) {
        try {
            ConcertService concertService = new ConcertServiceServant();
            Registry lookupService = LocateRegistry.createRegistry(8080);
            lookupService.rebind("concertService", concertService);
            System.out.println("Server up and running!");
        } catch (RemoteException e) {
            e.printStackTrace();
        }

    }
}
