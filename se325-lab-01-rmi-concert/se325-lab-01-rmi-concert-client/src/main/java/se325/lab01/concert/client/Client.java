package se325.lab01.concert.client;

import se325.lab01.concert.common.Concert;
import se325.lab01.concert.common.ConcertService;
import se325.lab01.concert.server.Keyboard;

import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.time.LocalDateTime;

public class Client {
    public static void main(String[] args) {

        try {
            Registry lookupService = LocateRegistry.getRegistry("localhost", 8080);
            ConcertService service = (ConcertService) lookupService.lookup("concertService");

            System.out.println("Options:");
            System.out.println("Add Concert (a)");
            System.out.println("Update Concert (u)");
            System.out.println("Delete Concert (d)");
            System.out.println("List Concerts (l)");
            System.out.println("Clear Concerts (c)");
            System.out.println("Quit (q)");
            System.out.println("");

            outer:
            while(true) {

                String option = Keyboard.prompt("Enter an option:");

                switch (option.charAt(0)) {
                    case 'a':
                        System.out.println();
                        String name = Keyboard.prompt("Enter a concert name:");
                        service.createConcert(new Concert(name, LocalDateTime.now()));
                        System.out.println();
                        break;
                    case 'u':
                        break;
                    case 'd':
                        System.out.println();
                        String id = Keyboard.prompt("Enter a concert ID to delete:");
                        service.deleteConcert(Long.parseLong(id));
                        System.out.println("");
                        break;
                    case 'l':
                        System.out.println();
                        for (Concert c : service.getAllConcerts()) {
                            System.out.println("ID: " + c.getId() + " Title: " + c.getTitle());
                        }
                        System.out.println();
                        break;
                    case 'c':
                        System.out.println();
                        service.clear();
                        System.out.println("All concerts have now been cleared.");
                        System.out.println();
                        break;
                    case 'q':
                        break outer;
                    default:
                        System.out.println();
                        System.out.println("Invalid option, choose from one of the available options a, u, d, l, c or q.");
                        System.out.println();
                        break;
                }


//                service.createConcert(new Concert(name, LocalDateTime.now()));
//
//                name = Keyboard.prompt("Enter a concert name:");
//
//                service.createConcert(new Concert(name, LocalDateTime.now()));
//
//                for (Concert c : service.getAllConcerts()) {
//                    System.out.println("ID: " + c.getId());
//                    System.out.println("Title: " + c.getTitle());
//                };
//
//                String id = Keyboard.prompt("Enter a concert ID to delete:");
//
//                service.deleteConcert(Long.parseLong(id));
            }


        } catch (RemoteException e) {
            e.printStackTrace();
        } catch (NotBoundException e) {
            e.printStackTrace();
        }
    }
}
